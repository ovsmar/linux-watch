import 'package:flutter/material.dart';

class DiskInfoWidget extends StatelessWidget {
  final String diskInfo;

  const DiskInfoWidget({Key? key, required this.diskInfo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<String> lines = diskInfo.split('\n');
    List<List<String>> data = lines.map((line) => line.split(RegExp(r'\s+'))).toList();

    // Assuming the first line is the header and the second line is the disk info
    List<String> diskLine = data.length > 2 ? data[2] : ['N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A'];

    String filesystem = diskLine[0];
    String size = diskLine[1];
    String used = diskLine[2];
    String available = diskLine[3];
    String usePercentage = diskLine[4];
    String mountedOn = diskLine[5];

    return Column(
      children: [
        Container(
          margin: EdgeInsets.all(10.0), // Add margin here
          child: Text(
            'Disk Information',
            style: Theme.of(context).textTheme.headline5,
          ),
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Wrap(
            spacing: 20,
            runSpacing: 20,
            children: [
              _buildCard('Filesystem', filesystem, Icons.storage, Colors.blueGrey.shade50),
              _buildCard('Size', size, Icons.storage, Colors.blueGrey.shade50),
              _buildCard('Used', used, Icons.storage, Colors.blueGrey.shade50),
              _buildCard('Available', available, Icons.storage, Colors.blueGrey.shade50),
              _buildCard('Use%', usePercentage, Icons.storage, Colors.blueGrey.shade50),
              _buildCard('Mounted on', mountedOn, Icons.storage, Colors.blueGrey.shade50),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildCard(String title, String value, IconData icon, Color color) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      elevation: 10,
      color: color,
      child: SizedBox(
        width: 250,
        height: 60,
        child: ListTile(
          leading: Icon(icon, size: 40),
          title: Text('$title: $value'),
        ),
      ),
    );
  }
}