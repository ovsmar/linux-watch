import 'package:flutter/material.dart';

class RamInfoWidget extends StatelessWidget {
  final String ramInfo;

  const RamInfoWidget({Key? key, required this.ramInfo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<String> lines = ramInfo.split('\n');
    List<List<String>> data = lines.map((line) => line.split(RegExp(r'\s+'))).toList();

    List<String> memLine = data.firstWhere((line) => line[0] == 'Mem:', orElse: () => ['Mem:', '0', '0', '0', '0', '0', '0']);

    String totalRam = memLine[1];
    String usedRam = memLine[2];
    String freeRam = memLine[3];
    String sharedRam = memLine[4];
    String buffersCache = memLine[5];
    String availableRam = memLine[6];

    return Column(
      children: [
        Container(
          margin: EdgeInsets.all(10.0), // Add margin here
          child: Text(
            'RAM Information',
            style: Theme.of(context).textTheme.headline5,
          ),
        ),
        Wrap(
          direction: Axis.horizontal,
          spacing: 10,
          runSpacing: 10,
          children: [
            _buildCard('Total RAM', totalRam, Icons.memory, Colors.blueGrey.shade50),
            _buildCard('Used RAM', usedRam, Icons.memory, Colors.blueGrey.shade50),
            _buildCard('Free RAM', freeRam, Icons.memory, Colors.blueGrey.shade50),
            _buildCard('Shared RAM', sharedRam, Icons.memory, Colors.blueGrey.shade50),
            _buildCard('Cache', buffersCache, Icons.memory, Colors.blueGrey.shade50),
            _buildCard('Available RAM', availableRam, Icons.memory, Colors.blueGrey.shade50),
          ],
        ),
      ],
    );
  }

  Widget _buildCard(String title, String value, IconData icon, Color color) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      elevation: 20,
      color: color,
      child: SizedBox(
        width: 300,
        height: 60,
        child: ListTile(
          leading: Icon(icon, size: 40),
          title: Text('$title: $value MB'),
        ),
      ),
    );
  }
}