import 'package:flutter/material.dart';

class SystemInfoWidget extends StatelessWidget {
  final String systemInfo;

  const SystemInfoWidget({Key? key, required this.systemInfo})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<String> lines = systemInfo.split('\n');
    List<List<String>> data = lines.map((line) => line.split(RegExp(r'\s+'))).toList();

    // Print the contents of the data list
    for (int i = 0; i < data.length; i++) {
      print('Index $i: ${data[i]}');
    }


    // Assuming the first line is the distribution info
    List<String> distributionLine = data.length > 0 ? data[0] : ['N/A'];
    String distributionInfo = distributionLine.join(' ');

    // Assuming the second line is the kernel info
    List<String> kernelLine = data.length > 1 ? data[1] : ['N/A'];
    String kernelInfo = kernelLine.join(' ');

    // Assuming the third line is the uptime
    List<String> uptimeLine = data.length > 4 ? data[4] : ['N/A'];
    String uptime = uptimeLine.join(' ');

    // Assuming the fourth line is the hostname
    List<String> processorLine = data.length > 15 ? data[15] : ['N/A'];
    String processor = processorLine.length > 5 ? processorLine.sublist(4).join(' ') : 'N/A';

    // Assuming the fifth line is the CPU info
    List<String> cpuLine = data.length > 4 ? data[4] : ['N/A'];
    String cpuInfo = cpuLine.join(' ');

    // Assuming the sixth line is the GPU info
    List<String> gpuLine = data.length > 5 ? data[5] : ['N/A'];
    String gpuInfo = gpuLine.join(' ');

    // Assuming the seventh line is the RAM size
    List<String> ramLine = data.length > 49 ? data[49] : ['N/A'];
    String ramSize = ramLine.length > 1 ? ramLine[1] : 'N/A';

    // Assuming the eighth line is the device name
    List<String> deviceNameLine = data.length > 6 ? data[6] : ['N/A'];
    String deviceName = deviceNameLine.join(' ');
    print('Device Name: $deviceName');

    // Assuming the ninth line is the hardware model
    List<String> hardwareModelLine = data.length > 8 ? data[8] : ['N/A'];
    String hardwareModel = hardwareModelLine.join(' ');

    // Assuming the tenth line is the disk info
    List<String> diskInfoLine = data.length > 9 ? data[9] : ['N/A'];
    String diskInfo = diskInfoLine.join(' ');

    // Assuming the eleventh line is the OS name
    List<String> osNameLine = data.length > 10 ? data[10] : ['N/A'];
    String osName = osNameLine.join(' ');

    // Assuming the twelfth line is the OS type
    List<String> osTypeLine = data.length > 11 ? data[11] : ['N/A'];
    String osType = osTypeLine.join(' ');

    // Assuming the thirteenth line is the GNOME version
    List<String> gnomeVersionLine = data.length > 69 ? data[69] : ['N/A'];
    String gnomeVersion = gnomeVersionLine.length > 2 ? gnomeVersionLine.sublist(2).join(' ') : 'N/A';

    // Assuming the fourteenth line is the window system
    List<String> windowSystemLine = data.length > 13 ? data[13] : ['N/A'];
    String windowSystem = windowSystemLine.join(' ');

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.all(10.0),
          child: Text(
            'System Information',
            style: Theme.of(context).textTheme.headline5,
          ),
        ),
        Wrap(
          alignment: WrapAlignment.start,
          spacing: 10,
          runSpacing: 10,
          children: [
            _buildCard('Distribution', distributionInfo, Icons.info, Colors.blueGrey.shade50),
            _buildCard('Kernel', kernelInfo, Icons.info, Colors.blueGrey.shade50),
            _buildCard('Uptime', uptime, Icons.info, Colors.blueGrey.shade50),
            _buildCard('Processor', processor, Icons.info, Colors.blueGrey.shade50),
            _buildCard('CPU Info', cpuInfo, Icons.info, Colors.blueGrey.shade50),
            _buildCard('GPU Info', gpuInfo, Icons.info, Colors.blueGrey.shade50),
            _buildCard('RAM Size', ramSize, Icons.info, Colors.blueGrey.shade50),
            _buildCard('Device Name', deviceName, Icons.info, Colors.blueGrey.shade50),
            _buildCard('Hardware Model', hardwareModel, Icons.info, Colors.blueGrey.shade50),
            _buildCard('Disk Info', diskInfo, Icons.info, Colors.blueGrey.shade50),
            _buildCard('OS Name', osName, Icons.info, Colors.blueGrey.shade50),
            _buildCard('OS Type', osType, Icons.info, Colors.blueGrey.shade50),
            _buildCard('GNOME Version', gnomeVersion, Icons.info, Colors.blueGrey.shade50),
            _buildCard('Window System', windowSystem, Icons.info, Colors.blueGrey.shade50),
          ],
        ),
      ],
    );
  }

Widget _buildCard(String title, String value, IconData icon, Color color) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      elevation: 10,
      color: color,
      child: SizedBox(
        width: 300,
        child: ListTile(
          leading: Icon(icon, size: 40),
          title: Text('$title: $value'),
        ),
      ),
    );
  }
}