import 'package:flutter/material.dart';

class TemperatureInfoWidget extends StatelessWidget {
  final String temperatureInfo;

  const TemperatureInfoWidget({super.key, required this.temperatureInfo});

  @override
  Widget build(BuildContext context) {
    List<String> lines = temperatureInfo.split('\n');
    String temp1 = lines.firstWhere((line) => line.contains('temp1:'), orElse: () => 'temp1: N/A');
    String cpuFan = lines.firstWhere((line) => line.contains('CPU Fan:'), orElse: () => 'CPU Fan: N/A');
    String videoFan = lines.firstWhere((line) => line.contains('Video Fan:'), orElse: () => 'Video Fan: N/A');
    String cpuTemp = lines.firstWhere((line) => line.contains('CPU:'), orElse: () => 'CPU: N/A');
    cpuTemp = cpuTemp.split('(').first.trim();

    return Column(
      children: [
        Container(
          margin: EdgeInsets.all(10.0), // Add margin here
          child: Text(
            'Temperature Information',
            style: Theme.of(context).textTheme.headline5,
          ),
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Wrap(
            direction: Axis.horizontal,
            spacing: 10,
            runSpacing: 10,
            children: [
              _buildCard('Temp1', temp1, Icons.thermostat, Colors.blueGrey.shade50),
              _buildCard('Temp1', cpuFan, Icons.toys, Colors.blueGrey.shade50),
              _buildCard('Video Fan', videoFan, Icons.videogame_asset, Colors.blueGrey.shade50),
              _buildCard('CPU Temp', cpuTemp, Icons.memory, Colors.blueGrey.shade50),
            ],
          ),
        ),
      ],
    );
  }

Widget _buildCard(String title, String value, IconData icon, Color color) {
  String actualValue = value.split(':').length > 1 ? value.split(':')[1] : value;

  return Card(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(15.0),
    ),
    elevation: 10,
    color: color,
    child: SizedBox(
      width: 300,
      height: 60,
      child: ListTile(
        leading: Icon(icon, size: 40),
        title: Text('$title: $actualValue'),
      ),
    ),
  );
}
}