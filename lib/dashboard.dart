import 'package:flutter/material.dart';

import 'package:linux_watch/pages/home/home_page.dart';
import 'package:linux_watch/responsive.dart';
import 'package:linux_watch/widget/menu.dart';

class DashBoard extends StatelessWidget {
  DashBoard({super.key});

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    bool isMobile = !Responsive.isDesktop(context);
    return Scaffold(
        key: _scaffoldKey,
        drawer: isMobile ? SizedBox(width: 250, child: Menu(scaffoldKey: _scaffoldKey)) : null,
        body: SafeArea(
          child: Row(
            children: [
              if (!isMobile)
                Expanded(
                  flex: 1,
                  child: SizedBox(
                      height: MediaQuery.of(context).size.height,
                      child: Menu(scaffoldKey: _scaffoldKey)),
                ),
              Expanded(flex: 8, child: HomePage(scaffoldKey: _scaffoldKey)),
            ],
          ),
        ));
  }
}