import 'package:flutter/material.dart';
import 'dart:async';
import '../service/disk_info.dart';
import '../service/ram_info.dart';
import '../service/system.info.dart';
import '../service/temp_info.dart';
import '../widget/OLDdisk_info_widget.dart';
import '../widget/OLDram_info_widget.dart';
import '../widget/OLDsystem_info_winget.dart';
import '../widget/OLDtemp_info_widget.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String ramInfo = '';
  String temperatureInfo = '';
  String diskInfo = '';
  String systemInfo = '';
  final RamInfo ramInfoService = RamInfo();
  final TemperatureInfo temperatureInfoService = TemperatureInfo();
  final DiskInfo diskInfoService = DiskInfo();
  final SystemInfo systemInfoService = SystemInfo();
  late Timer _timer;

  @override
  void initState() {
    super.initState();
    _loadRAMInfo();
    _loadTemperatureInfo();
    _loadDiskInfo();
    _loadSystemInfo();
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      _loadRAMInfo();
      _loadTemperatureInfo();
      _loadDiskInfo();
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  Future<void> _loadRAMInfo() async {
    String info = await ramInfoService.getRamInfo();
    setState(() {
      ramInfo = info;
    });
  }

  Future<void> _loadTemperatureInfo() async {
    String info = await temperatureInfoService.getTemperatureInfo();
    setState(() {
      temperatureInfo = info;
    });
  }

  Future<void> _loadDiskInfo() async {
    String info = await diskInfoService.getDiskInfo();
    setState(() {
      diskInfo = info;
    });
  }

  Future<void> _loadSystemInfo() async {
    String distributionInfo = await systemInfoService.getDistributionInfo();
    String kernelInfo = await systemInfoService.getKernelInfo();
    String uptime = await systemInfoService.getUptime();
    String hostname = await systemInfoService.getHostname();
    String cpuInfo = await systemInfoService.getCpuInfo();
    String gpuInfo = await systemInfoService.getGpuInfo();
    String ramSize = await systemInfoService.getRamSize();
    String deviceName = await systemInfoService.getDeviceName();
    String hardwareModel = await systemInfoService.getHardwareModel();
    String diskInfo = await systemInfoService.getDiskInfo();
    String osName = await systemInfoService.getOsName();
    String osType = await systemInfoService.getOsType();
    String gnomeVersion = await systemInfoService.getGnomeVersion();
    String windowSystem = await systemInfoService.getWindowSystem();

    setState(() {
      systemInfo =
          '$distributionInfo\n$kernelInfo\n$uptime\n$hostname\n$cpuInfo\n$gpuInfo\n$ramSize\n$deviceName\n$hardwareModel\n$diskInfo\n$osName\n$osType\n$gnomeVersion\n$windowSystem';
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: [
          RamInfoWidget(ramInfo: ramInfo),
          TemperatureInfoWidget(temperatureInfo: temperatureInfo),
          DiskInfoWidget(diskInfo: diskInfo),
          SystemInfoWidget(systemInfo: systemInfo),

          // Use the new widget here
        ],
      ),
    );
  }
}
