import 'dart:async';
import 'dart:io';

class TemperatureInfo {
  Future<String> getTemperatureInfo() async {
    ProcessResult result = await Process.run('sensors', []);
    if (result.exitCode == 0) {
      return result.stdout.toString();
    } else {
      return 'Error: Unable to retrieve temperature information.';
    }
  }
}