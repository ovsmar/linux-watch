import 'dart:async';
import 'dart:io';

class DiskInfo {
  Future<String> getDiskInfo() async {
    ProcessResult result = await Process.run('df', ['-h']);
    if (result.exitCode == 0) {
      return result.stdout.toString();
    } else {
      return 'Error: Unable to retrieve disk information.';
    }
  }
}