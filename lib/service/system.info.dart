import 'dart:async';
import 'dart:io';

class SystemInfo {
  Future<String> getDistributionInfo() async {
    ProcessResult result = await Process.run('lsb_release', ['-d']);
    if (result.exitCode == 0) {
      return result.stdout.toString();
    } else {
      return 'Error: Unable to retrieve distribution information.';
    }
  }

  Future<String> getKernelInfo() async {
    ProcessResult result = await Process.run('uname', ['-r']);
    if (result.exitCode == 0) {
      return result.stdout.toString();
    } else {
      return 'Error: Unable to retrieve kernel information.';
    }
  }

  Future<String> getUptime() async {
    ProcessResult result = await Process.run('uptime', ['-p']);
    if (result.exitCode == 0) {
      return result.stdout.toString();
    } else {
      return 'Error: Unable to retrieve uptime.';
    }
  }

  Future<String> getHostname() async {
    ProcessResult result = await Process.run('hostname', []);
    if (result.exitCode == 0) {
      return result.stdout.toString();
    } else {
      return 'Error: Unable to retrieve hostname.';
    }
  }

  Future<String> getCpuInfo() async {
    ProcessResult result = await Process.run('lscpu', []);
    if (result.exitCode == 0) {
      return result.stdout.toString();
    } else {
      return 'Error: Unable to retrieve CPU information.';
    }
  }

  Future<String> getGpuInfo() async {
    ProcessResult result =
        await Process.run('lspci', ['-vnn', '|', 'grep', 'VGA', '-A', '12']);
    if (result.exitCode == 0) {
      return result.stdout.toString();
    } else {
      return 'Error: Unable to retrieve GPU information.';
    }
  }

  Future<String> getRamSize() async {
    ProcessResult result = await Process.run('free', ['-h']);
    if (result.exitCode == 0) {
      return result.stdout.toString();
    } else {
      return 'Error: Unable to retrieve RAM size.';
    }
  }

  Future<String> getDeviceName() async {
    ProcessResult result = await Process.run('uname', ['-n']);
    if (result.exitCode == 0) {
      return result.stdout.toString();
    } else {
      return 'Error: Unable to retrieve device name.';
    }
  }

  Future<String> getHardwareModel() async {
    ProcessResult result = await Process.run(
        'cat', ['/proc/cpuinfo', '|', 'grep', 'model', '|', 'head', '-1']);
    if (result.exitCode == 0) {
      return result.stdout.toString();
    } else {
      return 'Error: Unable to retrieve hardware model.';
    }
  }

  Future<String> getGraphicsCard() async {
    ProcessResult result = await Process.run('lspci', ['|', 'grep', 'VGA']);
    if (result.exitCode == 0) {
      return result.stdout.toString();
    } else {
      return 'Error: Unable to retrieve graphics card information.';
    }
  }

  Future<String> getDiskInfo() async {
    ProcessResult result = await Process.run('df', ['-h']);
    if (result.exitCode == 0) {
      return result.stdout.toString();
    } else {
      return 'Error: Unable to retrieve disk information.';
    }
  }

  Future<String> getOsName() async {
    ProcessResult result = await Process.run('uname', ['-o']);
    if (result.exitCode == 0) {
      return result.stdout.toString();
    } else {
      return 'Error: Unable to retrieve OS name.';
    }
  }

  Future<String> getOsType() async {
    ProcessResult result = await Process.run('uname', ['-m']);
    if (result.exitCode == 0) {
      return result.stdout.toString();
    } else {
      return 'Error: Unable to retrieve OS type.';
    }
  }

  Future<String> getGnomeVersion() async {
    ProcessResult result = await Process.run('gnome-shell', ['--version']);
    if (result.exitCode == 0) {
      return result.stdout.toString();
    } else {
      return 'Error: Unable to retrieve GNOME version.';
    }
  }

  Future<String> getWindowSystem() async {
    ProcessResult result = await Process.run('echo', ['\$XDG_SESSION_TYPE']);
    if (result.exitCode == 0) {
      return result.stdout.toString();
    } else {
      return 'Error: Unable to retrieve window system.';
    }
  }
}
