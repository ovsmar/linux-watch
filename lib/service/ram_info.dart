import 'dart:async';
import 'dart:io';

class RamInfo {
  final StreamController<String> _ramInfoController = StreamController<String>();

  Stream<String> get ramInfoStream => _ramInfoController.stream;

  void startUpdatingRamInfo(Duration interval) {
    Timer.periodic(interval, (timer) async {
      String ramInfo = await getRamInfo();
      _ramInfoController.add(ramInfo);
    });
  }

  Future<String> getRamInfo() async {
    ProcessResult result = await Process.run('free', ['-m']);
    if (result.exitCode == 0) {
      return result.stdout.toString();
    } else {
      return 'Error: Unable to retrieve RAM information.';
    }
  }

  void dispose() {
    _ramInfoController.close();
  }
}
