import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../model/health_model.dart';
import '../../../service/ram_info.dart';
import '../../../widget/custom_card.dart';

class TempInfoCard extends StatelessWidget {
  final String temperatureInfo;

  const TempInfoCard({super.key, required this.temperatureInfo});

  @override
  Widget build(BuildContext context) {
    List<String> lines = temperatureInfo.split('\n');
    String temp1 = lines.firstWhere((line) => line.contains('temp1:'), orElse: () => 'temp1: N/A');
    temp1 = temp1.split(':')[1].trim();

    String cpuFan = lines.firstWhere((line) => line.contains('CPU Fan:'), orElse: () => 'CPU Fan: N/A');
    cpuFan = cpuFan.split(':')[1].trim();

    String videoFan = lines.firstWhere((line) => line.contains('Video Fan:'), orElse: () => 'Video Fan: N/A');
    videoFan = videoFan.split(':')[1].trim();

    String cpuTemp = lines.firstWhere((line) => line.contains('CPU:'), orElse: () => 'CPU: N/A');
    cpuTemp = cpuTemp.split(':')[1].trim();
    cpuTemp = cpuTemp.split('(').first.trim();

    final List<HealthModel> healthDetails = [
      HealthModel(icon: 'assets/svg/burn.svg', value: temp1, title: "Temp1"),
      HealthModel(icon: 'assets/svg/steps.svg', value: cpuFan, title: "Temp1"),
      HealthModel(icon: 'assets/svg/distance.svg', value: videoFan, title: "Video Fan"),
      HealthModel(icon: 'assets/svg/sleep.svg', value: cpuTemp, title: "CPU Temp"),

    ];

    return GridView.builder(
      itemCount: healthDetails.length,
      shrinkWrap: true,
      physics: const ScrollPhysics(),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 8, crossAxisSpacing: 15, mainAxisSpacing: 12.0),
      itemBuilder: (context, i) {
        return CustomCard(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SvgPicture.asset(healthDetails[i].icon),
              Padding(
                padding: const EdgeInsets.only(top: 15, bottom: 4),
                child: Text(
                  healthDetails[i].value,
                  style: const TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.w600),
                ),
              ),
              Text(
                healthDetails[i].title,
                style: const TextStyle(
                    fontSize: 13,
                    color: Colors.grey,
                    fontWeight: FontWeight.normal),
              ),
            ],
          ),
        );
      },
    );
  }
}
