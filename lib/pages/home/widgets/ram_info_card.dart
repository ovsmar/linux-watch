import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../model/health_model.dart';
import '../../../service/ram_info.dart';
import '../../../widget/custom_card.dart';

class RamInfoCard extends StatelessWidget {
  final String ramInfo;

  const RamInfoCard({super.key, required this.ramInfo});

  @override
  Widget build(BuildContext context) {
    List<String> lines = ramInfo.split('\n');
    List<List<String>> data = lines.map((line) => line.split(RegExp(r'\s+')))
        .toList();

    List<String> memLine = data.firstWhere((line) => line[0] == 'Mem:',
        orElse: () => ['Mem:', '0', '0', '0', '0', '0', '0']);

    String totalRam = memLine[1];
    String usedRam = memLine[2];
    String freeRam = memLine[3];
    String sharedRam = memLine[4];
    String buffersCache = memLine[5];
    String availableRam = memLine[6];


    final List<HealthModel> healthDetails = [
      HealthModel(icon: 'assets/svg/burn.svg', value: totalRam, title: "Total RAM"),
      HealthModel(icon: 'assets/svg/steps.svg', value: usedRam, title: "Used RAM"),
      HealthModel(icon: 'assets/svg/distance.svg', value: freeRam, title: "Free RAM"),
      HealthModel(icon: 'assets/svg/sleep.svg', value: sharedRam, title: "Shared RAM'"),
      HealthModel(icon: 'assets/svg/sleep.svg', value: buffersCache, title: "Cache"),
      HealthModel(icon: 'assets/svg/sleep.svg', value: availableRam, title: "Available RAM"),
    ];

    return GridView.builder(
      itemCount: healthDetails.length,
      shrinkWrap: true,
      physics: const ScrollPhysics(),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 8, crossAxisSpacing: 15, mainAxisSpacing: 12.0),
      itemBuilder: (context, i) {
        return CustomCard(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SvgPicture.asset(healthDetails[i].icon),
              Padding(
                padding: const EdgeInsets.only(top: 15, bottom: 4),
                child: Text(
                  healthDetails[i].value,
                  style: const TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.w600),
                ),
              ),
              Text(
                healthDetails[i].title,
                style: const TextStyle(
                    fontSize: 13,
                    color: Colors.grey,
                    fontWeight: FontWeight.normal),
              ),
            ],
          ),
        );
      },
    );
  }
}
