import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../model/health_model.dart';
import '../../../widget/custom_card.dart';

class SystemInfoCard extends StatelessWidget {
  final String systemInfo;

  const SystemInfoCard({super.key, required this.systemInfo});

  @override
  Widget build(BuildContext context) {
    List<String> lines = systemInfo.split('\n');
    List<List<String>> data = lines.map((line) => line.split(RegExp(r'\s+'))).toList();

    // Print the contents of the data list
    for (int i = 0; i < data.length; i++) {
      print('Index $i: ${data[i]}');
    }

    // Assuming the first line is the distribution info
    List<String> distributionLine = data.length > 0 ? data[0] : ['N/A'];
    String distributionInfo = distributionLine.join(' ');

    // Assuming the second line is the kernel info
    List<String> kernelLine = data.length > 1 ? data[1] : ['N/A'];
    String kernelInfo = kernelLine.join(' ');

    // Assuming the third line is the uptime
    List<String> uptimeLine = data.length > 4 ? data[4] : ['N/A'];
    String uptime = uptimeLine.join(' ');

    // Assuming the fourth line is the hostname
    List<String> processorLine = data.length > 15 ? data[15] : ['N/A'];
    String processor = processorLine.length > 5 ? processorLine.sublist(4).join(' ') : 'N/A';

    // Assuming the fifth line is the CPU info
    List<String> cpuLine = data.length > 4 ? data[4] : ['N/A'];
    String cpuInfo = cpuLine.join(' ');

    // Assuming the sixth line is the GPU info
    List<String> gpuLine = data.length > 5 ? data[5] : ['N/A'];
    String gpuInfo = gpuLine.join(' ');

    // Assuming the seventh line is the RAM size
    List<String> ramLine = data.length > 49 ? data[49] : ['N/A'];
    String ramSize = ramLine.length > 1 ? ramLine[1] : 'N/A';

    // Assuming the eighth line is the device name
    List<String> deviceNameLine = data.length > 6 ? data[6] : ['N/A'];
    String deviceName = deviceNameLine.join(' ');
    print('Device Name: $deviceName');

    // Assuming the ninth line is the hardware model
    List<String> hardwareModelLine = data.length > 8 ? data[8] : ['N/A'];
    String hardwareModel = hardwareModelLine.join(' ');

    // Assuming the tenth line is the disk info
    List<String> diskInfoLine = data.length > 9 ? data[9] : ['N/A'];
    String diskInfo = diskInfoLine.join(' ');

    // Assuming the eleventh line is the OS name
    List<String> osNameLine = data.length > 10 ? data[10] : ['N/A'];
    String osName = osNameLine.join(' ');

    // Assuming the twelfth line is the OS type
    List<String> osTypeLine = data.length > 11 ? data[11] : ['N/A'];
    String osType = osTypeLine.join(' ');

    // Assuming the thirteenth line is the GNOME version
    List<String> gnomeVersionLine = data.length > 69 ? data[69] : ['N/A'];
    String gnomeVersion = gnomeVersionLine.length > 2 ? gnomeVersionLine.sublist(2).join(' ') : 'N/A';

    // Assuming the fourteenth line is the window system
    List<String> windowSystemLine = data.length > 13 ? data[13] : ['N/A'];
    String windowSystem = windowSystemLine.join(' ');

    final List<HealthModel> healthDetails = [
      HealthModel(icon: 'assets/svg/burn.svg', value: distributionInfo, title: "Distribution"),
      HealthModel(icon: 'assets/svg/steps.svg', value: kernelInfo, title: "Kernel"),
      HealthModel(icon: 'assets/svg/distance.svg', value: uptime, title: "Uptime"),
      HealthModel(icon: 'assets/svg/sleep.svg', value: processor, title: "Processor"),
      HealthModel(icon: 'assets/svg/sleep.svg', value: cpuInfo, title: "CPU Info"),
      HealthModel(icon: 'assets/svg/sleep.svg', value: gpuInfo, title: "GPU Info"),
      HealthModel(icon: 'assets/svg/sleep.svg', value: ramSize, title: "RAM Size"),
      HealthModel(icon: 'assets/svg/sleep.svg', value: deviceName, title: "Device Name"),
      HealthModel(icon: 'assets/svg/sleep.svg', value: hardwareModel, title: "Hardware Model"),
      HealthModel(icon: 'assets/svg/sleep.svg', value: diskInfo, title: "Disk Info"),
      HealthModel(icon: 'assets/svg/sleep.svg', value: osName, title: "OS Name"),
      HealthModel(icon: 'assets/svg/sleep.svg', value: osType, title: "OS Type"),
      HealthModel(icon: 'assets/svg/sleep.svg', value: gnomeVersion, title: "GNOME Version"),
      HealthModel(icon: 'assets/svg/sleep.svg', value: windowSystem, title: "Window System"),
    ];

    return GridView.builder(
      itemCount: healthDetails.length,
      shrinkWrap: true,
      physics: const ScrollPhysics(),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 8, crossAxisSpacing: 15, mainAxisSpacing: 12.0),
      itemBuilder: (context, i) {
        return CustomCard(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SvgPicture.asset(healthDetails[i].icon),
              Padding(
                padding: const EdgeInsets.only(top: 15, bottom: 4),
                child: Text(
                  healthDetails[i].value,
                  style: const TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.w600),
                ),
              ),
              Text(
                healthDetails[i].title,
                style: const TextStyle(
                    fontSize: 13,
                    color: Colors.grey,
                    fontWeight: FontWeight.normal),
              ),
            ],
          ),
        );
      },
    );
  }
}

