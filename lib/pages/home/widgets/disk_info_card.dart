import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../model/health_model.dart';
import '../../../service/ram_info.dart';
import '../../../widget/custom_card.dart';

class DiskInfoCard extends StatelessWidget {
  final String diskInfo;

  const DiskInfoCard({super.key, required this.diskInfo});

  @override
  Widget build(BuildContext context) {
    List<String> lines = diskInfo.split('\n');
    List<List<String>> data = lines.map((line) => line.split(RegExp(r'\s+'))).toList();

    // Assuming the first line is the header and the second line is the disk info
    List<String> diskLine = data.length > 2 ? data[2] : ['N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A'];

    String filesystem = diskLine[0];
    String size = diskLine[1];
    String used = diskLine[2];
    String available = diskLine[3];
    String usePercentage = diskLine[4];
    String mountedOn = diskLine[5];

    final List<HealthModel> healthDetails = [
      HealthModel(icon: 'assets/svg/burn.svg', value: filesystem, title: "Filesystem"),
      HealthModel(icon: 'assets/svg/steps.svg', value: size, title: "Size"),
      HealthModel(icon: 'assets/svg/distance.svg', value: used, title: "Used"),
      HealthModel(icon: 'assets/svg/sleep.svg', value: available, title: "Available"),
      HealthModel(icon: 'assets/svg/sleep.svg', value: usePercentage, title: "Use%"),
      HealthModel(icon: 'assets/svg/sleep.svg', value: mountedOn, title: "Mounted on"),

    ];

    return GridView.builder(
      itemCount: healthDetails.length,
      shrinkWrap: true,
      physics: const ScrollPhysics(),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 8, crossAxisSpacing: 15, mainAxisSpacing: 12.0),
      itemBuilder: (context, i) {
        return CustomCard(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SvgPicture.asset(healthDetails[i].icon),
              Padding(
                padding: const EdgeInsets.only(top: 15, bottom: 4),
                child: Text(
                  healthDetails[i].value,
                  style: const TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.w600),
                ),
              ),
              Text(
                healthDetails[i].title,
                style: const TextStyle(
                    fontSize: 13,
                    color: Colors.grey,
                    fontWeight: FontWeight.normal),
              ),
            ],
          ),
        );
      },
    );
  }
}
