import 'dart:async';

import 'package:flutter/material.dart';
import 'package:linux_watch/pages/home/widgets/disk_info_card.dart';
import 'package:linux_watch/pages/home/widgets/ram_info_card.dart';
import 'package:linux_watch/pages/home/widgets/system_info_card.dart';
import 'package:linux_watch/pages/home/widgets/bar_graph_card.dart';
import 'package:linux_watch/pages/home/widgets/line_chart_card.dart';
import 'package:linux_watch/pages/home/widgets/temp_info_card.dart';

import '../../service/disk_info.dart';
import '../../service/ram_info.dart';
import '../../service/system.info.dart';
import '../../service/temp_info.dart';

class HomePage extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  const HomePage({Key? key, required this.scaffoldKey}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String ramInfo = '';
  String temperatureInfo = '';
  String diskInfo = '';
  String systemInfo = '';
  final RamInfo ramInfoService = RamInfo();
  final TemperatureInfo temperatureInfoService = TemperatureInfo();
  final DiskInfo diskInfoService = DiskInfo();
  final SystemInfo systemInfoService = SystemInfo();
  late Timer _timer;

  @override
  void initState() {
    super.initState();
    _loadRAMInfo();
    _loadTemperatureInfo();
    _loadDiskInfo();
    _loadSystemInfo();
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      _loadRAMInfo();
      _loadTemperatureInfo();
      _loadDiskInfo();
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  Future<void> _loadRAMInfo() async {
    String info = await ramInfoService.getRamInfo();
    setState(() {
      ramInfo = info;
    });
  }

  Future<void> _loadTemperatureInfo() async {
    String info = await temperatureInfoService.getTemperatureInfo();
    setState(() {
      temperatureInfo = info;
    });
  }

  Future<void> _loadDiskInfo() async {
    String info = await diskInfoService.getDiskInfo();
    setState(() {
      diskInfo = info;
    });
  }

  Future<void> _loadSystemInfo() async {
    String distributionInfo = await systemInfoService.getDistributionInfo();
    String kernelInfo = await systemInfoService.getKernelInfo();
    String uptime = await systemInfoService.getUptime();
    String hostname = await systemInfoService.getHostname();
    String cpuInfo = await systemInfoService.getCpuInfo();
    String gpuInfo = await systemInfoService.getGpuInfo();
    String ramSize = await systemInfoService.getRamSize();
    String deviceName = await systemInfoService.getDeviceName();
    String hardwareModel = await systemInfoService.getHardwareModel();
    String diskInfo = await systemInfoService.getDiskInfo();
    String osName = await systemInfoService.getOsName();
    String osType = await systemInfoService.getOsType();
    String gnomeVersion = await systemInfoService.getGnomeVersion();
    String windowSystem = await systemInfoService.getWindowSystem();

    setState(() {
      systemInfo =
          '$distributionInfo\n$kernelInfo\n$uptime\n$hostname\n$cpuInfo\n$gpuInfo\n$ramSize\n$deviceName\n$hardwareModel\n$diskInfo\n$osName\n$osType\n$gnomeVersion\n$windowSystem';
    });
  }

  @override
  Widget build(BuildContext context) {
    SizedBox height(BuildContext context) => const SizedBox(
          height: 30,
        );

    return SizedBox(
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
            child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18),
          child: Column(
            children: [
              const SizedBox(
                height: 18,
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: Text(
                  'System Information',
                  style: Theme.of(context).textTheme.headline5,
                ),
              ),
              SystemInfoCard(
                systemInfo: systemInfo,
              ),
              height(context),
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: Text(
                  'RAM Information',
                  style: Theme.of(context).textTheme.headline5,
                ),
              ),
              RamInfoCard(ramInfo: ramInfo),
              height(context),
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: Text(
                  'Temperature Information',
                  style: Theme.of(context).textTheme.headline5,
                ),
              ),
              TempInfoCard(temperatureInfo: temperatureInfo),
              height(context),
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: Text(
                  'Disk Information',
                  style: Theme.of(context).textTheme.headline5,
                ),
              ),
              DiskInfoCard(diskInfo: diskInfo),
              height(context),
              LineChartCard(),
              height(context),
              BarGraphCard(),
              height(context),
            ],
          ),
        )));
  }
}
